(ns plf04.core)

(defn stringE-1
  [xs]
  (letfn [(stringE [xs y]
            (if (empty? xs)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first xs))
                (stringE (rest xs) (inc y))
                (stringE (rest xs) y))))]
    (stringE xs 0)))

(stringE-1 "Hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele")
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")


(defn stringE-2
  [xs y]
  (letfn [(stringE [xs acc]
            (if (empty? xs)
              (if (and (> acc 0) (< acc 4))
                true false)
              (if (= \e (first xs))
                (stringE (rest xs) (+ acc 1))
                (stringE (rest xs) acc))))]
    (stringE xs y)))


(stringE-2 "Hello" 0)
(stringE-2 "Heelle" 0)
(stringE-2 "Heelele" 0)
(stringE-2 "Hll" 0)
(stringE-2 "e" 0)
(stringE-2 "" 0)
(defn stringTimes-1
  [xs y]
  (letfn [(stringTimes [xs y]
                       (if (== y 0)
                         ""
                         (str xs (stringTimes xs (dec y))))
           )]
    (stringTimes xs y)))

(stringTimes-1 "Hi" 2)
(stringTimes-1 "Hi" 3)
(stringTimes-1 "Hi" 1)
(stringTimes-1 "Hi" 0)
(stringTimes-1 "Hi" 5)
(stringTimes-1 "Oh Boy!" 2)
(stringTimes-1 "xs" 4)
(stringTimes-1 "" 4)
(stringTimes-1 "code" 2)
(stringTimes-1 "code" 3)

(defn stringTimes-2
  [xs y]
  (letfn [(stringTimes [xs y acc]
            (if (or (zero? y) (empty? xs))
              acc
              (str xs (stringTimes xs (dec y) (str acc)))))]
    (stringTimes xs y "")))


(stringTimes-2 "Hi" 2)
(stringTimes-2 "Hi" 3)
(stringTimes-2 "Hi" 1)
(stringTimes-2 "Hi" 0)
(stringTimes-2 "Hi" 5)
(stringTimes-2 "Oh Boy!" 2)
(stringTimes-2 "xs" 4)
(stringTimes-2 "" 4)
(stringTimes-2 "code" 2)
(stringTimes-2 "code" 3)

(defn frontTimes-1
  [lengt xs]
  (letfn [(frontTimes [xs y]
            (if (< (count xs) 3)
              (if (zero? y)
                ""
                (str lengt (frontTimes xs (dec y))))
              (if (zero? y)
                ""
                (str (subs lengt 0 3) (frontTimes xs (dec y))))))]
    (frontTimes lengt xs)))

(frontTimes-1 "Chocolate" 2)
(frontTimes-1 "Chocolate" 3)
(frontTimes-1 "Abc" 3)
(frontTimes-1 "Ab" 4)
(frontTimes-1 "A" 4)
(frontTimes-1 "" 4)
(frontTimes-1 "Abc" 0)

(defn frontTimes-2
  [lengt xs acc]
  (letfn [(frontTimes [xs y z]
            (if (< (count xs) 3)
              (if (zero? y)
                z
                (frontTimes xs (dec y) (str xs z)))
              (if (zero? y)
                z
                (frontTimes xs (dec y) (str (subs xs 0 3) z)))))]
    (frontTimes lengt xs acc)))

(frontTimes-2 "Chocolate" 2 "")
(frontTimes-2 "Chocolate" 3 "")
(frontTimes-2 "Abc" 3 "")
(frontTimes-2 "Ab" 4 "")
(frontTimes-2 "A" 4 "")
(frontTimes-2 "" 4 "")
(frontTimes-2 "Abc" 0 "")


(defn countXX-1
  [xs]
  (letfn [(countXX [y z]
            (if (empty? y)
              z
              (if (and (= \x (first y)) (= \x (first (rest y))))
                (countXX (rest y) (inc z))
                (countXX (rest y) z))))]
    (countXX xs 0)))

(countXX-1 "abcxx")
(countXX-1 "xxx")
(countXX-1 "xxxx")
(countXX-1 "abc")
(countXX-1 "Hello there")
(countXX-1 "Hexxo thxxe")
(countXX-1 "")
(countXX-1 "Kittens")
(countXX-1 "Kittensxxx")

(defn countXX-2
  [xs]
  (letfn [(countXX [y z acc]
            (if (empty? y)
              acc
              (if (and (= z (first y)) (= z (first (rest y))))
                (countXX (rest y) z (inc acc))
                (countXX (rest y) z acc))))]
    (countXX xs \x 0)))

(countXX-2 "abcxx")
(countXX-2 "xxx")
(countXX-2 "xxxx")
(countXX-2 "abc")
(countXX-2 "Hello there")
(countXX-2 "Hexxo thxxe")
(countXX-2 "")
(countXX-2 "Kittens")
(countXX-2 "Kittensxxx")


(defn stringSplosion-1
  [xs]
  (letfn [(stringSplosion [y]
            (if (== 0 (count y))
              y
              (apply str 
                     (stringSplosion 
                      (subs y 0 (- (count y) 1))) 
                     y)))]
    (stringSplosion xs)))

(stringSplosion-1 "Code")
(stringSplosion-1 "abc")
(stringSplosion-1 "ab")
(stringSplosion-1 "x")
(stringSplosion-1 "fade")
(stringSplosion-1 "there")
(stringSplosion-1 "Kitten")
(stringSplosion-1 "Bye")
(stringSplosion-1 "Good")
(stringSplosion-1 "Bad")

(defn stringSplosion-2
  [xs acc]
  (letfn [(stringSplosion [y acc]
            (if (== 0 (count y))
              acc
              (stringSplosion (subs y 0 
                              (- (count y) 1)) 
                              (str y acc))))]
    (stringSplosion xs acc)))


(stringSplosion-2 "Code" "")
(stringSplosion-2 "abc" "")
(stringSplosion-2 "ab" "")
(stringSplosion-2 "x" "")
(stringSplosion-2 "fade" "")
(stringSplosion-2 "there" "")
(stringSplosion-2 "Kitten" "")
(stringSplosion-2 "Bye" "")
(stringSplosion-2 "Good" "")
(stringSplosion-2 "Bad" "")


(defn array123-1
  [xs]
  (letfn [(array123 [xs]
            (if (and (= (first xs) 1) (= (first (rest xs)) 2) (= (first (rest (rest xs))) 3))
              true (
                    if (empty? xs) 
              false 
                    (array123 (rest xs)))))]
    (array123 xs)))

(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,4,1])
(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,1,2,3])
(array123-1 [1,1,2,1,2,1])
(array123-1 [1,2,3,1,2,3])
(array123-1 [1,2,3])
(array123-1 [1,1,1])
(array123-1 [1,2])
(array123-1 [1])
(array123-1 [])

(defn array123-2
  [xs]
  (letfn [(array123 [xs acc]
            (if (empty? xs)
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (and acc true)
                  (array123 (rest xs) acc))
                (array123 (empty xs) acc))))]
    (array123 xs "")))

(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,4,1])
(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,1,2,3])
(array123-2 [1,1,2,1,2,1])
(array123-2 [1,2,3,1,2,3])
(array123-2 [1,2,3])
(array123-2 [1,1,1])
(array123-2 [1,2])
(array123-2 [1])
(array123-2 [])


(defn stringX-1
  [xs]
  (letfn [(stringX [y z]
            (if (empty? y)
              ""
              (if (== 0 z)
                (str (first y) (stringX (rest y) (inc z)))
                (if (and (= \x (first y)) (> (count y) 1))
                  (stringX (rest y) (inc z))
                  (str (first y) (stringX (rest y) (inc z)))))))]
    (stringX xs 0)))

(stringX-1 "xxHxix")
(stringX-1 "abxxxcd")
(stringX-1 "xabxxxcdx")
(stringX-1 "xKittenx")
(stringX-1 "Hello")
(stringX-1 "xx")
(stringX-1 "x")
(stringX-1 "")

(defn stringX-2
  [xs]
  (letfn [(stringX [xs y acc]
            (if (empty? xs)
              acc
              (if (== 0 y)
                (str (first xs) (stringX (rest xs) (inc y) (str acc)))
                (if (and (= \x (first xs)) (> (count xs) 1))
                  (stringX (rest xs) (inc y) (str acc))
                  (str (first xs) (stringX (rest xs) (inc y) acc))))))]
    (stringX xs 0 "")))

(stringX-2 "xxHxix")
(stringX-2 "abxxxcd")
(stringX-2 "xabxxxcdx")
(stringX-2 "xKittenx")
(stringX-2 "Hello")
(stringX-2 "xx")
(stringX-2 "x")
(stringX-2 "")


(defn altPairs-1
[xs]
(letfn [(altPairs [xs y]
        
          (if (<= (count xs) y)
            ""
            (str (subs xs y (+ y 1)) (str (subs xs (+ y 1) (+ y 2))) (altPairs xs (+ y 4)))
            )
        )]
  (altPairs xs 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolates")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [xs]
  (letfn [(altPairs [xs y acc]

            (if (<= (count xs) y)
              acc
              (str (subs xs y (+ y 1)) (str (subs xs (+ y 1) (+ y 2))) (altPairs xs (+ y 4) acc))))]
    (altPairs xs 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolates")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOther")

(defn stringYak-1
  [xs]
  (letfn [(stringYak [xs y]
            (if (empty? xs)
              ""
              (if (and (= (first xs) \y) (= (first (rest xs)) \a) (= (first (rest (rest xs))) \k) (> (count xs) 1))
                (stringYak (rest (rest (rest xs))) (inc y))
                (str (first xs) (stringYak (rest xs) (inc y))))))]
    (stringYak xs 0)))

(stringYak-1 "yakpak")
(stringYak-1 "pakyak")
(stringYak-1 "yak123ya")
(stringYak-1 "yak")
(stringYak-1 "yakxxxyak")
(stringYak-1 "HiyakHi")
(stringYak-1 "xxxyakyyyakzzz")

(defn stringYak-2
  [xs]
  (letfn [(stringYak [xs y acc]
            (if (empty? xs)
              acc
              (if (and (= (first xs) \y) (= (first (rest xs)) \a) (= (first (rest (rest xs))) \k) (> (count xs) 1))
                (stringYak (rest (rest (rest xs))) (inc y) acc)
                (stringYak (rest xs) (inc y) (str acc (first xs))))))]
    (stringYak xs 0 "")))


(stringYak-2 "yakpak")
(stringYak-2 "pakyak")
(stringYak-2 "yak123ya")
(stringYak-2 "yak")
(stringYak-2 "yakxxxyak")
(stringYak-2 "HiyakHi")
(stringYak-2 "xxxyakyyyakzzz")


(defn has271-1
  [xs]
  (letfn [(f [xs]
            (if (or (<= (count xs) 2) (empty? xs))
              false
              (if (and
                   (== (first (rest xs)) (+ (first xs) 5))
                   (<= (if (pos? (- (first (rest (rest xs))) (dec (first xs))))
                         (- (first (rest (rest xs))) (dec (first xs)))
                         (* -1 (- (first (rest (rest xs))) (dec (first xs))))) 2))
                true
                (f (rest xs)))))]
    (f xs)))

(has271-1 [1,2,7,1])
(has271-1 [1, 2, 8, 1])
(has271-1 [2, 7, 1])
(has271-1 [3, 8, 2])
(has271-1 [2, 7, 3])
(has271-1 [2, 7, 4])
(has271-1 [2, 7, -1])
(has271-1 [2, 7, -2])
(has271-1 [4, 5, 3, 8, 0])
(has271-1 [2, 7, 5, 10, 4])
(has271-1 [2, 7, -2, 4, 9, 3])
(has271-1 [2, 7, 5, 10, 1])
(has271-1 [2, 7, -2, 4, 10, 2])
(has271-1 [1, 1, 4, 9, 0])
(has271-1 [1, 1, 4, 9, 4, 9, 2])


(defn has271-2
  [xs acc]
  (letfn [(f [xs acc]
            (if (empty? xs)
              false
              (if (>= (count xs) 3)
                (if (and (== (- (first (rest xs)) (first xs)) 5)
                         (>= 2 (if (neg? (- (first (rest (rest xs))) (- (first xs) 1)))
                                 (* -1 (- (first (rest (rest xs))) (- (first xs) 1)))
                                 (* 1 (- (first (rest (rest xs))) (- (first xs) 1))))))
                  true
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f xs acc)))

(has271-2 [1 2 7 1] "")
(has271-2 [1 2 8 1] "")
(has271-2 [2 7 1] "")
(has271-2 [3 8 2] "")
(has271-2 [2 7 3] "")
(has271-2 [2 7 4] "")
(has271-2 [2 7 -1] "")
(has271-2 [2 7 -2] "")
(has271-2 [4 5 3 8 0] "")
(has271-2 [2 7 5 10 4] "")
(has271-2 [2 7 -2 4 9 3] "")
(has271-2 [2 7 5 10 1] "")
(has271-2 [2 7 -2 4 10 2] "")
(has271-2 [1 1 4 9 0] "")
(has271-2 [1 1 4 9 4 9 2] "")
